const database = require("../../database");

class GetUser {
  async execute(id) {
    let result = false;
    try {
      const user = await database.User.findOne({
        where: { id },
        attributes: {
          exclude: ["password", "refreshToken", "token"],
        },
      });
      if (user) {
        result = user;
      } else {
        console.log("User does not exist");
      }
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}
module.exports = GetUser;
