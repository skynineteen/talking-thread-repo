module.exports = (sequelize, type) =>
  sequelize.define("room", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: type.STRING,
      unique: true,
    },
  });
