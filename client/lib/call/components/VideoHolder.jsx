import React, { Component } from "react";
import PropTypes from "prop-types";

class VideoHolder extends Component {
  async componentDidMount() {
    this.props.joinSession();
  }

  render() {
    return (
      <div id="video-holder">
        <div id="main-video" className="col-md-6">
          <p className="nickName" />
          <video autoPlay playsInline>
            <track kind="captions" />
          </video>
        </div>
        <div id="video-container" className="col-md-6" />
      </div>
    );
  }
}

export default VideoHolder;

VideoHolder.propTypes = {
  joinSession: PropTypes.func.isRequired,
};
