import { push } from "connected-react-router";
import localStorage from "../sessions/LocalStorageService";
import socket from "../sessions/sockets";
import { setSessionName } from "../main/actions";
import { OpenViduSessionService } from "../sessions";
import { showFlag } from "../shared/flag";
import { OPEN_SECTION } from "./actionsTypes";
import { resetMessages } from "../chat";

export const openSection = section => ({
  type: OPEN_SECTION,
  section,
});

export const leaveSession = () => async (dispatch, getState) => {
  if (OpenViduSessionService.session) {
    OpenViduSessionService.unpublish();
    OpenViduSessionService.leaveSession();
    socket.disconnect();
  }

  const {
    main: { sessionName },
  } = getState();
  dispatch(openSection(null));
  dispatch(resetMessages());
  dispatch(setSessionName(""));
  dispatch(push("/main"));
  dispatch(showFlag("success", `You successfully left ${sessionName} room`));
};

export const getUserData = () => (dispatch, getState) => {
  const user = getState().login.userReducer;
  const userName = OpenViduSessionService.session.connection.data.split(
    "%/%",
  )[0];
  const userData = {
    nickName: user.login,
    userName: JSON.parse(userName).clientData,
  };
  return userData;
};

export const joinSession = () => async (dispatch, getState) => {
  const user = getState().login.userReducer;
  const { sessionName } = getState().main;
  await OpenViduSessionService.joinSession(user, sessionName);
  if (user.login && user.publisher) {
    const userData = dispatch(getUserData());
    OpenViduSessionService.initPublisher(userData.nickName, userData.userName);
    dispatch(showFlag("success", `You've successfully joined ${sessionName}`));
  } else {
    // eslint-disable-next-line no-console
    console.warn("You don't have permissions to publish");
    OpenViduSessionService.initMainVideoThumbnail();
  }
  socket.connect(sessionName);
  socket.emit("userJoin", {
    id: localStorage.get("id"),
    login: user.login,
    role: user.role,
  });
};

export const shareScreen = () => dispatch => {
  OpenViduSessionService.unpublish();
  if (document.getElementById("data-main-videodata")) {
    document.getElementById("data-main-videodata").remove();
  }
  const userData = dispatch(getUserData());
  OpenViduSessionService.initPublisher(
    userData.nickName,
    userData.userName,
    "screen",
    error => {
      if (error.name === "SCREEN_EXTENSION_NOT_INSTALLED") {
        dispatch(
          showFlag(
            "error",
            "You should install an extension to be able to share your screen",
            "It can be found here for Chrome:",
            [
              {
                content: "OpenVidu ScreenSharing - Chrome Web Store",
                href:
                  "https://chrome.google.com/webstore/detail/openvidu-screensharing/lfcgfepafnobdloecchnfaclibenjold",
                target: "_blank",
              },
            ],
          ),
        );
      } else if (error.name === "SCREEN_SHARING_NOT_SUPPORTED") {
        dispatch(
          showFlag("error", "Your browser does not support screen sharing"),
        );
      } else if (error.name === "SCREEN_EXTENSION_DISABLED") {
        dispatch(
          showFlag("error", "You need to enable screen sharing extension"),
        );
      } else if (error.name === "SCREEN_CAPTURE_DENIED") {
        dispatch(
          showFlag(
            "error",
            "You need to choose a window or application to share",
          ),
        );
      }
    },
  );
};

export const unshareScreen = () => dispatch => {
  OpenViduSessionService.unpublish();
  if (document.getElementById("data-main-videodata")) {
    document.getElementById("data-main-videodata").remove();
  }
  const userData = dispatch(getUserData());
  OpenViduSessionService.initPublisher(userData.nickName, userData.userName);
};

export const openFullScreen = rootElement => () => {
  if (rootElement.requestFullscreen) {
    rootElement.requestFullscreen();
  } else if (rootElement.mozRequestFullScreen) {
    /* Firefox */
    rootElement.mozRequestFullScreen();
  } else if (rootElement.webkitRequestFullscreen) {
    /* Chrome, Safari and Opera */
    rootElement.webkitRequestFullscreen();
  } else if (rootElement.msRequestFullscreen) {
    /* IE/Edge */
    rootElement.msRequestFullscreen();
  }
};

export const closeFullScreen = () => () => {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    /* IE/Edge */
    document.msExitFullscreen();
  }
};

export const raiseHand = isToggled => () => {
  socket.emit("raiseHand", isToggled);
};
