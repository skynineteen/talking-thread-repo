import { connect } from "react-redux";
import { push } from "connected-react-router";
import { joinSession } from "../actions";

import VideoHolder from "../components/VideoHolder";

const mapStateToProps = state => ({
  user: {
    login: state.login.userReducer.login,
    password: state.login.userReducer.password,
    publisher: state.login.userReducer.publisher,
  },
  sessionName: state.main.sessionName,
});

const mapDispatchToProps = dispatch => ({
  joinSession: () => dispatch(joinSession()),
  push: path => dispatch(push(path)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VideoHolder);
