import { connect } from "react-redux";
import { push } from "connected-react-router";

import CallPage from "../components/CallPage";

const mapStateToProps = state => ({
  user: {
    login: state.login.userReducer.login,
    password: state.login.userReducer.password,
    publisher: state.login.userReducer.publisher,
  },
  section: state.call.section,
});

export default connect(
  mapStateToProps,
  {
    push,
  },
)(CallPage);
