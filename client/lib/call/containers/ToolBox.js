import { connect } from "react-redux";
import { openSection, raiseHand } from "../actions";

import ToolBox from "../components/ToolBox";

const mapStateToProps = state => ({
  section: state.call.section,
});

const mapDispatchToProps = dispatch => ({
  onOpenSection: section => dispatch(openSection(section)),
  raiseHand: isToggled => dispatch(raiseHand(isToggled)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ToolBox);
