import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  leaveSession,
  openFullScreen,
  closeFullScreen,
  shareScreen,
  unshareScreen,
} from "../actions";

import ToolBar from "../components/ToolBar";

const mapDispatchToProps = dispatch => ({
  push,
  openFullScreen: element => dispatch(openFullScreen(element)),
  closeFullScreen: () => dispatch(closeFullScreen()),
  leaveSession: () => dispatch(leaveSession()),
  shareScreen: () => dispatch(shareScreen()),
  unshareScreen: () => dispatch(unshareScreen()),
});

export default connect(
  null,
  mapDispatchToProps,
)(ToolBar);
