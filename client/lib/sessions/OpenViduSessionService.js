import $ from "jquery";
import APIService from "./APIService";

class OpenViduSessionService {
  constructor() {
    this.session = null;
    this.sessionName = null; // Name of the video session the user will connect to
    this.token = null; // Token retrieved from OpenVidu Server
    this.publisher = null;
    this.OV = new window.OpenVidu();
  }

  async joinSession(user, sessionName) {
    return new Promise(async (resolve, reject) => {
      await this.connect(user, sessionName, token => {
        this.sessionName = sessionName;
        this.session = this.OV.initSession();

        // On every new Stream received...
        this.session.on("streamCreated", event => {
          // Subscribe to the Stream to receive it
          // HTML video will be appended to element with 'video-container' id
          const subscriber = this.session.subscribe(
            event.stream,
            "video-container",
          );

          // When the HTML video has been appended to DOM...
          subscriber.on("videoElementCreated", videoCreatedEvent => {
            // Add a new HTML element for the user's name and nickname over its video
            this.appendUserData(
              videoCreatedEvent.element,
              subscriber.stream.connection,
            );
          });
        });

        // On every Stream destroyed...
        this.session.on("streamDestroyed", event => {
          // Delete the HTML element with the user's name and nickname
          this.removeUserData(event.stream.connection);
        });

        // --- 4) Connect to the session passing the retrieved token and some more data from
        //        the client (in this case a JSON with the nickname chosen by the user) ---

        const nickName = user.login;
        return this.session
          .connect(token, { clientData: nickName })
          .then(resolve)
          .catch(error => {
            reject(error);

            // eslint-disable-next-line no-console
            console.warn(
              "There was an error connecting to the session:",
              error.code,
              error.message,
            );
          });
      });
    });
  }

  leaveSession() {
    // Leave the session by calling 'disconnect' method over the Session object ---

    this.session.disconnect();
    this.session = null;
    this.sessionName = null;
  }

  cleanMainVideo() {
    $("#main-video video").get(0).srcObject = null;
    $("#main-video p").each(() => {
      $(this).html("");
    });
  }

  async connect(user, sessionName, callback) {
    // Video-call chosen by the user

    let body;
    let response;
    try {
      try {
        response = await APIService.get(
          `/api/rooms/${sessionName}`,
          {},
          "Request of finding room gone wrong",
        );
      } catch (err) {
        response = false;
      }

      if (response !== false) {
        body = { login: user.login };
        response = await APIService.post(
          `api/rooms/${sessionName}/connect`,
          { body },
          "Request of CONNECTION gone WRONG",
        );
      } else {
        body = { user: { login: user.login }, room: { roomName: sessionName } };
        response = await APIService.post(
          "api/rooms/",
          { body },
          "Request of room gone WRONG:",
        );
        body = { login: user.login };
        console.log(response.name);
        response = await APIService.post(
          `api/rooms/${response.name}/connect`,
          { body },
          "Request of CONNECTION gone WRONG",
        );
      }

      // eslint-disable-next-line prefer-destructuring
      this.token = response.token; // Get token from response
      // eslint-disable-next-line no-console
      console.warn(`Request of TOKEN gone WELL (TOKEN:${this.token})`);
      callback(this.token); // Continue the join operation
    } catch (e) {
      // eslint-disable-next-line no-console
      console.warn("Error. Can't getToken", e);
    }
  }

  initPublisher(userName, nickName, videoSource, callback) {
    // --- 6) Get your own camera stream ---

    this.publisher = this.OV.initPublisher(
      "video-container",
      {
        audioSource: undefined, // The source of audio. If undefined default microphone
        videoSource, // The source of video. If undefined default webcam
        publishAudio: true, // Whether you want to start publishing with your audio unmuted or not
        publishVideo: true, // Whether you want to start publishing with your video enabled or not
        resolution: "1280x720", // The resolution of your video
        frameRate: 30, // The frame rate of your video
        insertMode: "APPEND", // How the video is inserted in the target element 'video-container'
        mirror: true, // Whether to mirror your local video or not
      },
      callback,
    );

    // --- 7) Specify the actions when events take place in our publisher ---

    // When our HTML video has been added to DOM...
    this.publisher.on("videoElementCreated", event => {
      // Init the main video with ours and append our data
      const userData = {
        nickName,
        userName,
      };
      this.initMainVideo(event.element, userData);
      this.appendUserData(event.element, userData);
      $(event.element).prop("muted", true); // Mute local video
    });

    // --- 8) Publish your stream ---

    this.session.publish(this.publisher);
  }

  unpublish() {
    this.session.unpublish(this.publisher);
    this.publisher = null;
  }

  appendUserData(videoElement, connection) {
    let connectionData;
    if (connection.nickName) {
      // Appending local video data
      connectionData = {
        client: connection.nickName,
        server: connection.userName,
        nodeId: "main-videodata",
      };
    } else {
      connectionData = {
        client: JSON.parse(connection.data.split("%/%")[0]).clientData,
        server: JSON.parse(connection.data.split("%/%")[1]).serverData.login,
        nodeId: connection.connectionId,
      };
    }

    const dataNode = document.createElement("div");

    dataNode.className = "data-node";
    dataNode.id = `data-${connectionData.nodeId}`;
    dataNode.innerHTML = `<p class='nickName'>${connectionData.client}</p>`;
    videoElement.parentNode.insertBefore(dataNode, videoElement.nextSibling);
    this.addClickListener(
      videoElement,
      connectionData.client,
      connectionData.server,
    );
  }

  removeUserData(connection) {
    const userNameRemoved = $(`#data-${connection.connectionId}`);
    if (
      $(userNameRemoved)
        .find("p.userName")
        .html() === $("#main-video p.userName").html()
    ) {
      this.cleanMainVideo(); // The participant focused in the main video has left
      const anotherVideoCaption = document.querySelector(
        `#video-container div:not([id='data-${connection.connectionId}'])`,
      );
      const anotherVideo = anotherVideoCaption.previousElementSibling;
      const userName = anotherVideoCaption.querySelector(".nickName").innerHTML;
      this.initMainVideo(anotherVideo, {
        nickName: userName,
        userName,
      });
    }
    $(`#data-${connection.connectionId}`).remove();
  }

  initMainVideo = (videoElement, userData) => {
    $("#main-video video").get(0).srcObject = videoElement.srcObject;
    $("#main-video p.nickName").html(userData.nickName);
    $("#main-video p.userName").html(userData.userName);
    $("#main-video video").prop("muted", true);
  };

  initMainVideoThumbnail = () => {
    $("#main-video video").css(
      "background",
      "url('images/subscriber-msg.jpg') round",
    );
  };

  isPublisher = userName => userName.includes("publisher");

  addClickListener = (videoElement, clientData, serverData) => {
    videoElement.addEventListener("click", () => {
      const mainVideo = $("#main-video video").get(0);
      if (mainVideo.srcObject !== videoElement.srcObject) {
        $("#main-video").fadeOut("fast", () => {
          $("#main-video p.nickName").html(clientData);
          $("#main-video p.userName").html(serverData);
          mainVideo.srcObject = videoElement.srcObject;
          $("#main-video").fadeIn("fast");
        });
      }
    });
  };

  runStream = async (user, sessionName) => {
    await this.joinSession(user, sessionName);
    // Here we check somehow if the user has 'PUBLISHER' role before
    // trying to publish its stream. Even if someone modified the client's code and
    // published the stream, it wouldn't work if the token sent in Session.connect
    // method is not recognized as 'PUBLIHSER' role by OpenVidu Server

    const userName = this.session.connection.data.split("%/%")[0];
    const userData = {
      nickName: user.login,
      userName: JSON.parse(userName).clientData,
    };

    this.initPublisher(userData.nickName, userData.userName);
  };
}

export default new OpenViduSessionService();
