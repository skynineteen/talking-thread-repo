import React, { Fragment } from "react";
import PropTypes from "prop-types";

import Message from "./Message";

const MessageList = props => {
  const messages = props.messages.map((message, index) => (
    // eslint-disable-next-line react/no-array-index-key
    <Message key={index} author={message.author} msgBody={message.msgBody} />
  ));
  return <Fragment>{messages}</Fragment>;
};

MessageList.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object),
};

export default MessageList;
