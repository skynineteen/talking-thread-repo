import React, { Component } from "react";
import Textfield from "@atlaskit/textfield";
import Button from "@atlaskit/button";
import PropTypes from "prop-types";

export default class InputField extends Component {
  sendMsg = e => {
    e.preventDefault();
    const msgBody = document.getElementById("m").value;
    if (msgBody) {
      this.props.addMessage(msgBody);
      document.getElementById("m").value = "";
    }
  };

  render() {
    return (
      <form className="chatForm" onSubmit={this.sendMsg}>
        <Textfield
          className="chatFormInput"
          id="m"
          placeholder="Type here..."
        />
        <Button
          className="sendMsgButton"
          appearance="primary"
          onClick={this.sendMsg}
        >
          Send
        </Button>
      </form>
    );
  }
}

InputField.propTypes = {
  addMessage: PropTypes.func,
};
