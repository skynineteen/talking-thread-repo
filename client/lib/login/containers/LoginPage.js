import { connect } from "react-redux";
import {
  loginRequest,
  openSignInForm,
  openSignUpForm,
  openSignInModal,
  closeSignInModal,
  createAccountRequest,
} from "../actions";
import LoginPage from "../components/LoginPage";

const mapStateToProps = state => ({
  formState: state.login.formReducer,
});

const mapDispatchToProps = dispatch => ({
  onLogin: (login, password) => dispatch(loginRequest(login, password)),
  onOpenSignInForm: () => dispatch(openSignInForm()),
  onOpenSignUpForm: () => dispatch(openSignUpForm()),
  onOpenSignInModal: () => dispatch(openSignInModal()),
  onCloseSignInModal: () => dispatch(closeSignInModal()),
  createAccount: data => dispatch(createAccountRequest(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPage);
