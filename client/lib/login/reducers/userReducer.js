import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
} from "../actionTypes";

const initialState = {
  login: "",
  password: "",
  publisher: true,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        login: action.payload.login,
        password: action.payload.password,
      };
    case LOGIN_ERROR:
      return { ...state };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        login: "",
        password: "",
        publisher: true,
      };
    case LOGOUT_ERROR:
      return state;
    default:
      return state;
  }
}
