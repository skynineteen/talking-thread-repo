import React, { Fragment } from "react";
import PropTypes from "prop-types";
import TextField from "@atlaskit/textfield";
import Form, {
  Field,
  HelperMessage,
  ErrorMessage,
  ValidMessage,
} from "@atlaskit/form";

// eslint-disable-next-line arrow-body-style
const isPasswordEqual = (value, firstPass) => {
  if (value !== firstPass || value === "") {
    return "Password and password confirm should be identical";
  }
  return undefined;
};

const SignUpForm = props => {
  let firstPass;
  return (
    <div
      style={{
        display: "flex",
        width: "400px",
        margin: "0 auto",
        flexDirection: "column",
      }}
    >
      <Form
        onSubmit={data => {
          props.createAccount(data);
          return false;
        }}
      >
        {({ formProps }) => (
          <form {...formProps} id="signUpFormId">
            <Field
              className="signUp-form signUp-form__userName-input"
              id="userName"
              name="userName"
              isRequired
              defaultValue=""
              label="Name"
              validate={value =>
                /* eslint-disable-next-line implicit-arrow-linebreak */
                value.length < 2 ? "Please use 2 or more characters" : undefined
              }
            >
              {/* eslint-disable-next-line implicit-arrow-linebreak */}
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="text" autoComplete="off" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Use 2 or more characters (letters, numbers or periods)
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Login needs to be no less than 2 characters
                    </ErrorMessage>
                  )}
                  {/* eslint-disable-next-line react/no-unescaped-entities */}
                  {meta.valid && <ValidMessage>That's Ok</ValidMessage>}
                </Fragment>
              )}
            </Field>
            <Field
              className="signUp-form signUp-form__userLogin-input"
              id="userLogin"
              name="userLogin"
              isRequired
              defaultValue=""
              label="Login"
              validate={value =>
                /* eslint-disable-next-line implicit-arrow-linebreak */
                value.length < 5 ? "Please use 5 or more characters" : undefined
              }
            >
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="text" autoComplete="off" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Use 5 or more characters (letters, numbers or periods)
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Login needs to be no less than 5 characters
                    </ErrorMessage>
                  )}
                  {/* eslint-disable-next-line react/no-unescaped-entities */}
                  {meta.valid && <ValidMessage>That's Ok</ValidMessage>}
                </Fragment>
              )}
            </Field>
            <Field
              name="userPassword"
              className="signUp-form signUp-form__userPassword-input"
              id="userPassword"
              label="Password"
              defaultValue=""
              isRequired
              validate={value =>
                /* eslint-disable-next-line implicit-arrow-linebreak */
                value.length < 4 ? "Please use 4 or more characters" : undefined
              }
            >
              {/* eslint-disable-next-line no-return-assign */}
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField
                    onChange={(firstPass = fieldProps.value)}
                    type="password"
                    {...fieldProps}
                  />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Use 4 or more characters with a mix of letters, numbers or
                      symbols
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Password needs to be no less than 4 characters
                    </ErrorMessage>
                  )}
                  {meta.valid && <ValidMessage>Awesome password!</ValidMessage>}
                </Fragment>
              )}
            </Field>
            <Field
              name="userPasswordConfirm"
              className="signUp-form signUp-form__userPasswordCheck-input"
              id="userPasswordConfirm"
              label="Confirm password"
              defaultValue=""
              isRequired
              validate={value => isPasswordEqual(value, firstPass)}
            >
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="password" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Please enter the password once again
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Password needs to be the same as the previous one
                    </ErrorMessage>
                  )}
                  {meta.valid && (
                    <ValidMessage>Password is confirmed</ValidMessage>
                  )}
                </Fragment>
              )}
            </Field>
          </form>
        )}
      </Form>
    </div>
  );
};

SignUpForm.propTypes = {
  createAccount: PropTypes.func,
};

export default SignUpForm;
