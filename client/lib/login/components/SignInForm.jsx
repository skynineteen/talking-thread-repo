import React, { Fragment } from "react";
import PropTypes from "prop-types";
import Button from "@atlaskit/button";
import TextField from "@atlaskit/textfield";
import Form, {
  Field,
  HelperMessage,
  ErrorMessage,
  ValidMessage,
} from "@atlaskit/form";

const SignInForm = props => {
  const { openSignUpForm, onLogin } = props;

  return (
    <div
      style={{
        display: "flex",
        width: "400px",
        margin: "0 auto",
        flexDirection: "column",
      }}
    >
      <Form
        className="signIn-form jumbotron"
        onSubmit={formData => {
          onLogin(formData.login, formData.password);
        }}
      >
        {({ formProps }) => (
          <form {...formProps} id="signInFormId">
            <Field
              className="signIn-form signIn-form__login-input"
              id="login"
              name="login"
              isRequired
              defaultValue=""
              label="Login"
              validate={value =>
                /* eslint-disable-next-line implicit-arrow-linebreak */
                value.length < 5 ? "Please use 5 or more characters" : undefined
              }
            >
              {/* eslint-disable-next-line no-confusing-arrow */}
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="text" autoComplete="off" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Use 5 or more characters (letters, numbers or periods)
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Login needs to be no less than 5 characters
                    </ErrorMessage>
                  )}
                  {/* eslint-disable-next-line react/no-unescaped-entities */}
                  {meta.valid && <ValidMessage>That's Ok</ValidMessage>}
                </Fragment>
              )}
            </Field>
            <Field
              name="password"
              className="signIn-form signIn-form__password-input"
              id="password"
              label="Password"
              defaultValue=""
              isRequired
              validate={value =>
                /* eslint-disable-next-line implicit-arrow-linebreak */
                value.length < 4 ? "Please use 4 or more characters" : undefined
              }
            >
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="password" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Use 4 or more characters with a mix of letters, numbers or
                      symbols
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Password needs to be no less than 4 characters
                    </ErrorMessage>
                  )}
                  {meta.valid && <ValidMessage>Awesome password!</ValidMessage>}
                </Fragment>
              )}
            </Field>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            <span
              style={{
                display: "inline-block",
                width: "240px",
                textAlign: "right",
                paddingRight: "20px",
                paddingLeft: "20px",
                paddingTop: "50px",
              }}
            >
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              Haven't got an account yet?
            </span>
            <Button
              style={{
                display: "inline-block",
                textAlign: "right",
              }}
              appearance="link"
              onClick={openSignUpForm}
            >
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              Create, it's free
            </Button>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
          </form>
        )}
      </Form>
    </div>
  );
};

export default SignInForm;

SignInForm.propTypes = {
  openSignUpForm: PropTypes.func,
  onLogin: PropTypes.func,
};
