import React, { Component } from "react";
import { push } from "connected-react-router";
import PropTypes from "prop-types";

class Join extends Component {
  state = {
    sessionName: "",
  };

  setSessionName = e => {
    this.setState({ sessionName: e.target.value });
  };

  render() {
    return (
      <div className="section section--contact" id="contact">
        <h2 className="heading-section heading-section--slider">
          Buzy mobile game
        </h2>
        <h3 className="subheading-section">Download game</h3>
        <form
          onSubmit={e => {
            e.preventDefault();
            this.props.dispatch(push(`/${this.state.sessionName}`));
          }}
        >
          <input
            type="text"
            className="input"
            placeholder="Enter Your Room"
            onChange={this.setSessionName}
          />
          <button className="button--landing button--contact" type="submit">
            Join
          </button>
        </form>
      </div>
    );
  }
}

export default Join;

Join.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
