import uuidv4 from "uuid/v4";
import { SHOW_FLAG, DISMISS_FLAG } from "./actionsTypes";

export const dismissFlag = id => ({
  type: DISMISS_FLAG,
  id,
});

export const showFlag = (
  typeOfFlag,
  title,
  description,
  actions,
) => dispatch => {
  const id = uuidv4();
  dispatch({
    type: SHOW_FLAG,
    id,
    typeOfFlag,
    title,
    description,
    actions,
  });

  setTimeout(() => dispatch(dismissFlag(id)), 5000);
};
