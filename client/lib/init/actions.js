import { push } from "connected-react-router";
import IS_LOADING from "./actionTypes";
import { APIService } from "../sessions";
import LocalStorageService from "../sessions/LocalStorageService";
import {
  loginSuccess,
  loginError,
  logoutSuccess,
  openSignInModal,
} from "../login/actions";
import { setSessionName } from "../main/actions";

export function isLoading(boolean) {
  return {
    type: IS_LOADING,
    isLoading: boolean,
  };
}

function isCallPage(pathname) {
  return pathname !== "/" && pathname !== "/login" && pathname !== "/main";
}

export const isUserLogOutInNextWindow = () => dispatch => {
  window.onstorage = e => {
    if (e.newValue === null && e.key === "accessToken") {
      if (
        // eslint-disable-next-line operator-linebreak
        window.location.pathname !== "/" ||
        window.location.pathname !== "/login"
      ) {
        dispatch(logoutSuccess());
        dispatch(push("/"));
      }
    }
  };
};

export const isUserLoggedIn = pathname => async dispatch => {
  const accessToken = LocalStorageService.get("accessToken");
  const id = LocalStorageService.get("id");

  if (isCallPage(pathname)) {
    dispatch(setSessionName(pathname.slice(1)));
  }

  if (accessToken && id) {
    try {
      const result = await APIService.get(`api/users/${id}`, "Can't get user");
      dispatch(loginSuccess(result.login, ""));

      if (!isCallPage(pathname)) {
        dispatch(push("/main"));
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.warn("Error. Can't logIn", e);
      dispatch(loginError());

      LocalStorageService.delete("accessToken");
      LocalStorageService.delete("id");
      dispatch(push("/"));
    }
  } else if (pathname === "/main") {
    dispatch(push("/"));
  } else if (isCallPage(pathname)) {
    dispatch(openSignInModal());
  }

  dispatch(isLoading(false));
};
