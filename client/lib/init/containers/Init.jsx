import { connect } from "react-redux";
import { isUserLoggedIn, isUserLogOutInNextWindow } from "../actions";
import Init from "../components/Init";

const mapStateToProps = state => ({
  isLoading: state.init.isLoading,
  pathname: state.router.location.pathname,
});

const mapDispatchToProps = dispatch => ({
  onLoading: path => dispatch(isUserLoggedIn(path)),
  isUserLogOutInNextWindow: () => dispatch(isUserLogOutInNextWindow()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Init);
